
-- SUMMARY --

Menu Sync open menu options based on the path being presented.

For a full description of the module, visit the project page:
  http://drupal.org/project/menusync

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/menusync


-- REQUIREMENTS --

None


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* All configuration is done at <http://www.example.org/admin/content/menusync>


-- TROUBLESHOOTING --

Empty.


-- FAQ --

Q: Empty

A: Empty.
